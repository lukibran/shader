﻿Shader "Custom/Bump_Disp" {
	Properties {
		_Color ("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap ("Normal Map", 2D) = "bump" {}
		_BumpDepth ("Bumpiness", Range(0.1,3.0)) = 1
		_SpecColor ("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess("Shininess", Float) = 10
		_DispMap("Disp Map", 2D) = "disp"{}
		_DispHeight("Disp Height", Range(0.00,0.01)) = 0.005
		_DispRef("Disp Ref", Range(0.0,10.0)) = 1
		_DispStepCnt("Disp Step Cnt", Range(0.0,10.0)) = 1
	}
	SubShader {
		Pass{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0
			#pragma exclude_renderers flash
			
			//Deklarierte Variablen
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform float4 _BumpMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _BumpDepth;
			uniform sampler2D _DispMap;
			uniform float _DispHeight;
			uniform float _DispRef;
			uniform float _DispStepCnt;
			
			//Unity Varaiblen
			uniform float4 _LightColor0;
			
			//Input Stucts
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
			};
			
			//Vertex Funktion
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize( mul(float4(v.normal,0.0), _World2Object).xyz);
				o.tangentWorld = normalize( mul( _Object2World, v.tangent).xyz);
				o.binormalWorld = normalize( cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				o.posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				
				return o;
			}
			
			//Fragment Funktion
			float4 frag(vertexOutput i) : COLOR
			{
				float3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 lightDirection;
				float atten;
				
				//Directional Light
				if(_WorldSpaceLightPos0.w == 0.0){
					atten = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}
				else{
					float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
					float distance = length(fragmentToLightSource);
					atten = 1.0/distance;
					lightDirection = normalize(fragmentToLightSource);
				}
				
				//Displacement Mapping Berechnungen
				float2 DispTex = i.tex; 
			    float2 dUV = -viewDirection.xy * _DispHeight;
			    float prev_hits = 0.0;
			    float h = 1.0;
			    float hit_h = 0;
			    
			    //erster Durchlauf 
			    for (int it = 0; it < _DispStepCnt; ++it){ 
				     h -= 0.1;
				     DispTex += dUV;
				     float h_tex = tex2D(_DispMap, DispTex.xy).r;
				     float is_first_hit = saturate((h_tex - h - prev_hits) * 4999999);
				     hit_h += is_first_hit * h;
				     prev_hits += is_first_hit;
			    }
			    
			    DispTex = i.tex + dUV * (1.0f-hit_h) * 10.0f - dUV;
			    float2 Temp = DispTex;
			    h = hit_h+0.1f;
			    float Start = h;
			    dUV *= 0.2f;
			    prev_hits = 0.0f;
			    hit_h = 0.0f;
			    
			    //zweiter Durchlauf genauer
			    for(int it = 0;it < _DispRef ; ++it)
			    {
				     h -= 0.02f;
				     DispTex += dUV;
				     float CurrentHeight = tex2D(_DispMap, DispTex.xy).r;
				     float first_hit = saturate((CurrentHeight - h - prev_hits) * 499999.0);
				     hit_h += first_hit * h;
				     prev_hits += first_hit;
			    }
			    DispTex = Temp + dUV * (Start - hit_h) * 50.0f;
			    
				//Texture Maps
				float4 tex = tex2D(_MainTex, DispTex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float4 texN = tex2D(_BumpMap, DispTex.xy * _BumpMap_ST.xy + _BumpMap_ST.zw);
				
				//unpackNormal Funktion
				float3 localCoord = float3(2.0 * texN.ag - float2(1.0,1.0), 0.0);
				localCoord.z  = _BumpDepth;
				
				//normal Transpose matrix
				float3x3 local2WorldTranspose = float3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//Calculate normal direction
				float3 normalDirection = normalize( mul( localCoord, local2WorldTranspose));
				
				//Lighting 
				float3 diffuseReflection = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection));
				float3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
		
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.xyz + diffuseReflection + specularReflection;
				
				
				
				return float4(tex.xyz * lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
	}
	//FallBack "Specular"
}
