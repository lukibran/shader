﻿Shader "Custom/Bump_Disp" {
	Properties {
		_Color ("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap ("Normal Map", 2D) = "bump" {}
		_BumpDepth ("Bumpiness", Range(0.1,3.0)) = 1
		_SpecColor ("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess("Shininess", Float) = 10
		_SelfShadowing("SelfShadow",Range(0.0,1.0)) = 1

	}
	SubShader {
		Pass{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash
			
			//Deklarierte Variablen
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform float4 _BumpMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _BumpDepth;
			
			//Unity Varaiblen
			uniform float4 _LightColor0;
			
			//Input Stucts
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
			};
			
			//Vertex Funktion
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize( mul(float4(v.normal,0.0), _World2Object).xyz);
				o.tangentWorld = normalize( mul( _Object2World, v.tangent).xyz);
				o.binormalWorld = normalize( cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				o.posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				
				return o;
			}
			
			//Fragment Funktion
			float4 frag(vertexOutput i) : COLOR
			{
				float3 ViewDir = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 LightDir;
				float atten;
				
				//Directional Light
				if(_WorldSpaceLightPos0.w == 0.0){
					atten = 1.0;
					LightDir = normalize(_WorldSpaceLightPos0.xyz);
				}
				else{
					float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
					float distance = length(fragmentToLightSource);
					atten = 1.0/distance;
					LightDir = normalize(fragmentToLightSource);
				}
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float4 texN = tex2D(_BumpMap, i.tex.xy * _BumpMap_ST.xy + _BumpMap_ST.zw);
				
				//unpackNormal Funktion
				float3 localCoord = float3(2.0 * texN.ag - float2(1.0,1.0), 0.0);
				localCoord.z  = _BumpDepth;
				
				//normal Transpose matrix
				float3x3 local2WorldTranspose = float3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//Calculate normal direction
				float3 normalDirection = normalize( mul( localCoord, local2WorldTranspose));
				
				//Lighting 
				float3 diff = atten * _LightColor0.xyz * saturate(dot(normalDirection, LightDir));
				float3 specularReflection = diff * _SpecColor.xyz * pow(saturate(dot(reflect(-LightDir, normalDirection), ViewDir)), _Shininess);
				
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.xyz + diff + specularReflection;
							
				return float4(tex.xyz * lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
		// 2ter Pass für Punktlichtquelle da dieser in Unity nur mit ForwardAdd gemacht werden kann
		// Hier wird im Fragment Shader nur das Licht zurückgegeben keine Textur 
		Pass{
			Tags { "LightMode" = "ForwardAdd" }
			Blend One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash
			
			//Deklarierte Variablen
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform float4 _BumpMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _BumpDepth;
			
			
			//Unity Varaiblen
			uniform float4 _LightColor0;
			
			//Input Stucts
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
			};
			
			//Vertex Funktion
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize( mul(float4(v.normal,0.0), _World2Object).xyz);
				o.tangentWorld = normalize( mul( _Object2World, v.tangent).xyz);
				o.binormalWorld = normalize( cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				o.posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				
				return o;
			}
			
			//Fragment Funktion
			float4 frag(vertexOutput i) : COLOR
			{
				float3 ViewDir = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 LightDir;
				float atten;
				
				//Directional Light
				if(_WorldSpaceLightPos0.w == 0.0){
					atten = 1.0;
					LightDir = normalize(_WorldSpaceLightPos0.xyz);
				}
				else{
					float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
					float distance = length(fragmentToLightSource);
					atten = 1.0/distance;
					LightDir = normalize(fragmentToLightSource);
				}
				
				//Texture Maps
				float4 texN = tex2D(_BumpMap, i.tex.xy * _BumpMap_ST.xy + _BumpMap_ST.zw);
				
				//unpackNormal Funktion
				float3 localCoord = float3(2.0 * texN.ag - float2(1.0,1.0), 0.0);
				localCoord.z  = _BumpDepth;
				
				//normal Transpose matrix
				float3x3 local2WorldTranspose = float3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//Calculate normal direction
				float3 normalDirection = normalize( mul( localCoord, local2WorldTranspose));
				
				//Lighting 
				float3 diff = atten * _LightColor0.xyz * saturate(dot(normalDirection, LightDir));
				float3 specularReflection = diff * _SpecColor.xyz * pow(saturate(dot(reflect(-LightDir, normalDirection), ViewDir)), _Shininess);
				
				float3 lightFinal = diff + specularReflection;
					
				return float4(lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
	}
	//FallBack "Specular"
}
